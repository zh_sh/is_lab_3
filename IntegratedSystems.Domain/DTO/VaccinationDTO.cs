﻿using IntegratedSystems.Domain.Domain_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegratedSystems.Domain.DTO
{
    public class VaccinationDTO
    {
        public List<Patient>? patients { get; set; }
        public List<String>? manufacturers { get; set; } /*{
            get { return this.manufacturers; }
            set
            {
                new List<string>()
                    {
                        "Manufacturer1", "Manufacturer2", "Manufacturer3", "Manufacturer4"
                    };
            }
        }*/
        public Guid patientId { get; set; }
        public DateTime vaccinationDate { get; set; }
        public Guid vaccCenterId { get; set; }
        public String? manufacturer {  get; set; }
    }
}
