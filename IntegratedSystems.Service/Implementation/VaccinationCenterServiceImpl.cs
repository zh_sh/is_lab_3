﻿using IntegratedSystems.Domain.Domain_Models;
using IntegratedSystems.Repository.Interface;
using IntegratedSystems.Service.Interface;


namespace IntegratedSystems.Service.Implementation
{
    public class VaccinationCenterServiceImpl : IVaccinationCenterService
    {

        private readonly IRepository<VaccinationCenter> vaccinationCenterRepository;

        public VaccinationCenterServiceImpl(IRepository<VaccinationCenter> repository)
        {
            vaccinationCenterRepository = repository;
        }

        public VaccinationCenter CreateVaccinationCenter(VaccinationCenter vaccinationCenter)
        {
            return vaccinationCenterRepository.Insert(vaccinationCenter);
        }

        public VaccinationCenter DeleteVaccinationCenter(Guid id)
        {
            var vaccCenter = this.GetVaccinationCenterById(id);
            return vaccinationCenterRepository.Delete(vaccCenter);
        }

        public VaccinationCenter GetVaccinationCenterById(Guid? id)
        {
            return vaccinationCenterRepository.Get(id);
        }

        public List<VaccinationCenter> GetVaccinationCenters()
        {
            return vaccinationCenterRepository.GetAll().ToList();
        }

        public void DecrementCapacity(Guid id)
        {
            var vaccCenter = this.GetVaccinationCenterById(id);
            vaccCenter.MaxCapacity -= 1;
            this.UpdateExistingVaccinationCenter(vaccCenter);
        }

        public VaccinationCenter UpdateExistingVaccinationCenter(VaccinationCenter vaccinationCenter)
        {
            return vaccinationCenterRepository.Update(vaccinationCenter);
        }
    }
}
