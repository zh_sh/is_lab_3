﻿using IntegratedSystems.Domain.Domain_Models;
using IntegratedSystems.Domain.DTO;
using IntegratedSystems.Repository.Interface;
using IntegratedSystems.Service.Interface;

namespace IntegratedSystems.Service.Implementation
{
    public class VaccinationServiceImpl : IVaccinationService
    {
        private readonly IRepository<VaccinationCenter> vaccinationCenterRepository;
        private readonly IRepository<Vaccine> vaccineRepository;

        public VaccinationServiceImpl(IRepository<VaccinationCenter> repository, IRepository<Vaccine> vaccineRepository)
        {
            vaccinationCenterRepository = repository;
            this.vaccineRepository = vaccineRepository;
        }

        public void ScheduleVaccine(VaccinationDTO dto)
        {
            Vaccine vaccine = new Vaccine
            {
                Manufacturer = dto.manufacturer,
                Certificate = Guid.NewGuid(),
                VaccinationCenter = dto.vaccCenterId,
                PatientId = dto.patientId,
                DateTaken = dto.vaccinationDate,
                Center = vaccinationCenterRepository.Get(dto.vaccCenterId)
            };

            vaccineRepository.Insert(vaccine);
        }

        public List<Vaccine> GetVaccinesForCenter(Guid? centerId)
        {
            return vaccineRepository.GetAll().Where(z => z.VaccinationCenter == centerId).ToList();
        }

        public List<Vaccine> GetVaccinesForPatient(Guid? patientId)
        {
            return vaccineRepository.GetAll().Where(z => z.PatientId == patientId).ToList();
        }
    }
}
