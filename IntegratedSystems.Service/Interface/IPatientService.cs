﻿using IntegratedSystems.Domain.Domain_Models;


namespace IntegratedSystems.Service.Interface
{
    public interface IPatientService
    {
        public List<Patient> GetPatients();
        public Patient GetPatientById(Guid? id);
        public Patient CreatePatient(Patient patient);
        public Patient UpdateExistingPatient(Patient patient);
        public Patient DeletePatient(Guid id);

    }
}
