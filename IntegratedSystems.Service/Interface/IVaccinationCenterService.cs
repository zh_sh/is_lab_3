﻿using IntegratedSystems.Domain.Domain_Models;

namespace IntegratedSystems.Service.Interface
{
    public interface IVaccinationCenterService
    {
        public List<VaccinationCenter> GetVaccinationCenters();
        public VaccinationCenter GetVaccinationCenterById(Guid? id);
        public VaccinationCenter CreateVaccinationCenter(VaccinationCenter vaccinationCenter);
        public VaccinationCenter UpdateExistingVaccinationCenter(VaccinationCenter vaccinationCenter);
        public VaccinationCenter DeleteVaccinationCenter(Guid id);

        public void DecrementCapacity(Guid id);
    }
}
